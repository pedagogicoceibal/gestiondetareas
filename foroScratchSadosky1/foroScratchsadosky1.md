# Mensajes del foro de Scratch Sadosky 1 27/11

## Vanesa Josefina Ester RodriguezMie 15 May, 2019 at 1:29 pm

Buenas tardes, tal como veniamos hablando en el foro. Comparto la maqueta para editar destinado a grupos que se incian en Scratch para que armen la animación del problema. Diferenciando tres momentos de planificación de la animación con tres escenarios y dos objetos. Muy simple! la idea es que editen y modifiquen, pueden agregar y sacar. Lo empecé a implementar para los cuando hacen la actividad de la propuesta 3 y 4 en el programa.

Nos leemos...
[Archivo Animacion_en_Scratch.sb2](Archivo Animacion_en_Scratch.sb2) 1 MB


### Mónica RegueiroMie 15 May, 2019 at 9:11 pm

buenisimo. Yo justo estoy preparando PT3! Ya lo miro
Me gusta 's comment · Responder

### E. MichalekMar 2 Jul, 2019 at 3:08 pm

Me parece muy bueno lo de resaltar que debemos tener un borrador o una guía antes de comenzar a programar. Yo utilizo una actividad creada en Ardora que, con algunas de las imagenes del Scratch, les permite elegir el modo de comenzar, los personajes, y escribir los diálogos. Sobre eso hacern una captura de pantalla de lo armado para tener de guía despues. Así no se pierden tanto entre los bloques a momento de programar.

## Sergio ArciénagaVie 17 May, 2019 at 12:32 am

Saludos, estas son dos simulaciones en Scratch pensadas para reforzar el trabajo con la ficha de activar ideas y la infografía de ciudades inteligentes.

La actividad sería:

1) Con Scratch transformar la infografía de ciudad inteligente en una rueda de la fortuna, que jugando les permita cuantificar los distintos aspectos del problema en relación a las ocho categorías propuestas en la infografía. Es decir pensar y evaluar mi problema a la luz de dichas categorías. Este sketch se llama ActivandoIdeas.sb2 

2) Con la cuantificación vamos a cargar los datos para que el Mamífero volador me diga qué tan significativo puede llegar a ser el problema. Este sketch se llama EvaluandProblemas.sb2

Saludos

 
Mostrar menos
Archivo ActivandoIdeas.sb2120 KB
Archivo EvaluandoProblemas.sb260 KB
ActivandoIdeas.flv
ActivandoIdeas.flv48 MB
EvaluandoProblemas.flv
EvaluandoProblemas.flv17 MB
Me gusta 's comment2 · Responder
Ocultar las 2 respuestas
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZMar 21 May, 2019 at 5:12 pm

Muy bueno!!!  Gracias!
Me gusta 's comment · Responder
Imagen de perfil de Daniela Rosa Saldaña
Daniela Rosa SaldañaMie 22 May, 2019 at 9:12 am

Genial!!!
Me gusta 's comment · Responder
Imagen de perfil de Maria Eugenia Lopez Rueda
Maria Eugenia Lopez RuedaEditado · Lun 20 May, 2019 at 2:49 pm

Comparto aquí mi experiencia. Considero de suma importancia que al programar se ordenen primero las ideas. Trabajo con el concepto de algoritmo en todos los grupos. Antes de realizar la actividad en Scratch, pensamos las acciones. En los grupos que recién se inician, en la tercera semana conocen Scratch y experimentan con el entorno. A partir de la cuarta, comienzo a trabajar el concepto de algoritmo. En los grupos que ya programan en Scratch, trabajamos con el concepto de modularización. Utilizando la paleta de bloque Más Bloques. Comienzo la clase mostrando al gatito, contándoles lo que quiero que haga, esto lo realizo en un editor de texto. Luego comenzamos, entre TODOS, a darle forma en Scratch. Por último, los invito a realizar algo similar, eligiendo ellos el personaje, el fondo y las acciones. 
Mostrar menos
Archivo adjunto de imagen
Archivo adjunto de imagen
Me gusta 's comment2 · Responder
Imagen de perfil de Maria Eugenia Lopez Rueda
Maria Eugenia Lopez RuedaLun 20 May, 2019 at 3:21 pm

Con los alumnos más chiquitos, introduzco el tema disfraces, dibujando. Crean un personaje, una carita feliz,  creamos un disfraz para mostrar el cambio de emocion, una carita triste. De este modo, les presento las herramientas para crear y/o editar los objetos. Algo que probablemente necesiten más adelante. Y queda registrado qué son los disfraces y cómo deben hacer para cambiarlos. Comparto unos trabajitos 
Mostrar menos
Archivo adjunto de imagen
Archivo adjunto de imagen
Me gusta 's comment3 · Responder
Ocultar las 2 respuestas
Imagen de perfil de Marta Montiel
Marta MontielLun 20 May, 2019 at 5:48 pm

Hermosa idea, lo voy a tomar para los niños de 4to. Gracias por compartilo.

Dejo mi ejemplo de EvaluandoElProblema, basado en el ejemplo de Sergio, no soy muy innovadora, pero ya con el tiempo calculo iré cambiando :)

Saludos
Archivo EvaluandoElProblema.sb2251 KB
Me gusta 's comment · Responder
Imagen de perfil de Sergio Arciénaga
Sergio ArciénagaMie 22 May, 2019 at 7:07 am

Me parece muy bueno trabajar el tema de los disfraces desde los emoticones. Gracias
Me gusta 's comment · Responder
Imagen de perfil de Mónica Regueiro
Mónica RegueiroMar 21 May, 2019 at 3:45 pm

Gisele compartio este material muy interesante:

Les dejo dos videos cortitos que encontré que explican en Scratch 1

1) https://www.youtube.com/watch?v=35Vezg8Q5x4

2) https://www.youtube.com/watch?v=ImMEygYraFY

Y la página: https://aprendescratch.com/ tiene ejemplos y explicaciones
Me gusta 's comment1 · Responder
Imagen de perfil de Brenda Paz
Brenda PazMar 21 May, 2019 at 4:23 pm

Hola Compañeros!!

Necesito de su colaboración. Tengo una de las escuelas que cuando los alumnos suben lo que realizaron en Scratch en el foro de Crea, al querer abrirlo desde el archivo les sale el mensaje que se muestra en la foto. Por el momento lo solucionamos abriendo la programación realizada desde Sratch. El resto de los compañeros y yo podemos abrir el archivo en Crea sin problemas.

Desde ya muchas gracias.

Besos,

 

Brenda
Mostrar menos
Archivo adjunto de imagen
Me gusta 's comment · Responder
Ocultar las 4 respuestas
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZMar 21 May, 2019 at 5:19 pm

Brenda, entiendo que es para que lo abran directamente desde el foro?.... porque interpreto que si descargan el archivo de programaciòn y lo abren desde scratch lo pueden ver?

 

 
Me gusta 's comment · Responder
Imagen de perfil de Brenda Paz
Brenda PazMie 22 May, 2019 at 12:35 pm

Sí Adriana, un grupo de alumnas quieren subir el archivo al foro y brirlo desde ahí, pero les sale este error. Desde Scratch lo pueden abrir y ver perfectamente, el problema es en el foro y sólo les sucede a ellas, el resto lo puede abrir sin problemas. Sabras qué puede ser que este sucediendo?? Muchas gracias!!!
Me gusta 's comment · Responder
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZMie 5 Jun, 2019 at 4:26 pm

Pediles que lo guarden y suban en formato video.  Contanos después que pasa. 

Saludos, 
Me gusta 's comment · Responder
Imagen de perfil de Brenda Paz
Brenda PazJue 6 Jun, 2019 at 10:29 am

Finalmente, quedamos que abren sus programaciones desde Scrach.Voy a proponerles que lo suban en formato video. 

Muchas gracias. Besos!
Me gusta 's comment · Responder
Imagen de perfil de Maria Checa
Maria ChecaMar 21 May, 2019 at 5:20 pm

Hola a todos!!! feliz con esta experiencia!!! en principio con algunas dificultades pero hoy bastante bien gracias a Dios, a ustedes que a traves de lo que escriben aprendo muchisimo, a las DA y a los niños que son unos unos soles!!! 

Ahora vere la maqueta!!! pronto compartire alguna tarea,... Gracias por compartir y legarme tanto!! Gracias a todo el Equipo Pedagógico y  Técnico por la predisposición de siempre!!!
Mostrar menos
Me gusta 's comment1 · Responder
Imagen de perfil de Mauro Martinez
Mauro MartinezMie 5 Jun, 2019 at 7:01 pm

Buenas! Como estan? comparto animación de Scratch que utilice para un grupo que eligio como problemática la contaminación. Quizas le sirve de idea o lo puedan utilizar.

Luego les subi otro archivo similar en blanco para que ellos lo puedan programar.

 
Archivo Proyecto_Robotito.sb2192 KB
Me gusta 's comment2 · Responder
Ocultar las 2 respuestas
Imagen de perfil de E. Michalek
E. MichalekVie 7 Jun, 2019 at 6:19 pm

buenísimo!! ya también trabajé con la basura, pero el robot la hace desaparecer!! para la próxima tiene que averiguar cómo "reciclarla"
Me gusta 's comment · Responder
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZJue 20 Jun, 2019 at 7:20 am

Gracias Mauro por compartir el proyecto 
Me gusta 's comment · Responder
Imagen de perfil de Mauro Martinez
Mauro MartinezEditado · Mie 5 Jun, 2019 at 7:10 pm

Comparto algunos escenarios creados por un grupo, es una buena propuesta para iniciar con los mas peques.
Archivo adjunto de imagen
Archivo adjunto de imagen
Archivo adjunto de imagen
Archivo adjunto de imagen
Me gusta 's comment1 · Responder
Ocultar 1 respuesta
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZJue 20 Jun, 2019 at 7:20 am

Muy buena idea Mauro. A los m{as pequeños los atrapa la idea de crear personajes y escenarios. Gracias por compartirlo.  Saludos, Adriana 
Me gusta 's comment · Responder
Imagen de perfil de Ariel Solovey
Ariel SoloveyJue 6 Jun, 2019 at 12:28 pm

Hola compañeros, les comparto aquí una producción de un grupo de chiquilines donde se trabaja movimiento utilizando un ascensor. Les puede ser de utilidad para reforzar algunos conceptos.

Saludos
Archivo movimiento_acensor.sb245 KB
Me gusta 's comment2 · Responder
Ocultar las 2 respuestas
Imagen de perfil de Sergio Arciénaga
Sergio ArciénagaMar 18 Jun, 2019 at 10:32 pm

Buenísimo el ejemplo para trabajar el tema de enviar y recibir mensajes. Muchas gracias Ariel
Me gusta 's comment1 · Responder
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZJue 20 Jun, 2019 at 7:21 am

Hola Ariel, muy buen ejemplo. 

 
Me gusta 's comment · Responder
Imagen de perfil de Alvar Leandro Maciel
Alvar Leandro MacielEditado · Dom 9 Jun, 2019 at 12:36 pm

Hola a todes. Les comparto el procedimiento para instalar scratch2 en Linux. Yo lo hice en un Debian 9 stable. Pero debe funcionar para todos los Debian y derivades.

Esta es la fuente https://askubuntu.com/questions/913892/how-to-install-scratch-2-on-ubuntu-16-10-or-17-04-64bit

pero le metií algunas modificaciones porque el último scratch2 no arrancaba

 

# Habilitar la instalación de librerias i386 en 64 bits
sudo dpkg --add-architecture i386
# instalar las librerias i386 necesarias
sudo apt-get install libgtk2.0-0:i386 libstdc++6:i386 libxml2:i386 libxslt1.1:i386 libcanberra-gtk-module:i386 gtk2-engines-murrine:i386 libqt4-qt3support:i386 libgnome-keyring0:i386 libnss-mdns:i386 libnss3:i386

# Hacer que todo sea visible para Adobe Air
sudo ln -s /usr/lib/i386-linux-gnu/libgnome-keyring.so.0 /usr/lib/libgnome-keyring.so.0
sudo ln -s /usr/lib/i386-linux-gnu/libgnome-keyring.so.0.2.0 /usr/lib/libgnome-keyring.so.0.2.0

# Bajar Adobe Air
cd ~/Descargas
wget http://airdownload.adobe.com/air/lin/download/2.6/AdobeAIRSDK.tbz2
$ sudo mkdir /opt/adobe-air-sdk
$ sudo tar jxf AdobeAIRSDK.tbz2 -C /opt/adobe-air-sdk

# Bajar Air runtime/SDK desde Archlinux
wget https://aur.archlinux.org/cgit/aur.git/snapshot/adobe-air.tar.gz
$ sudo tar xvf adobe-air.tar.gz -C /opt/adobe-air-sdk
$ sudo chmod +x /opt/adobe-air-sdk/adobe-air/adobe-air

# Bajar la versión de scratch que me anduvo bien
$ sudo mkdir /opt/adobe-air-sdk/scratch
wget https://scratch.mit.edu/scratchr2/static/sa/Scratch-456.0.1.air
$ sudo cp Scratch-455.air /opt/adobe-air-sdk/scratch/
cp Scratch-456.0.1.air /tmp/
cd /tmp/
unzip /tmp/Scratch-456.0.1.air
$ sudo cp /tmp/icons/AppIcon128.png /opt/adobe-air-sdk/scratch/scratch.png


# Y este es el archivo .desktop que hay qie hacer para que esté en el menú


cat << _EOF_ > /usr/share/applications/Scratch2.desktop
[Desktop Entry]
Encoding=UTF-8
Version=1.0
Type=Application
Exec=/opt/adobe-air-sdk/adobe-air/adobe-air /opt/adobe-air-sdk/scratch/Scratch-456.0.1.air
Icon=/opt/adobe-air-sdk/scratch/scratch.png
Terminal=false
Name=Scratch 2
Comment=Programming system and content development tool
Categories=Application;Education;Development;ComputerScience;
MimeType=application/x-scratch-project
_EOF_

# y después


sudo chmod +x /usr/share/applications/Scratch2.desktop

Mostrar menos
Me gusta 's comment3 · Responder
Imagen de perfil de Sergio Arciénaga
Sergio ArciénagaJue 13 Jun, 2019 at 12:34 pm

Estimadas/os:

Comparto tutorial para ver de solucionar el problema de que los .sb2 no se abren en Scratch 2 en Ubuntu, cuando le hacemos doble clic. Seguramente se lo puede mejorar así que están todos invitados a modificar y mejorar lo que consideren. Saludos
Archivo Tutorial.sb2_.odt718 KBVER
Archivo adjunto de imagen
Me gusta 's comment · Responder
Imagen de perfil de Sergio Arciénaga
Sergio ArciénagaEditado · Mar 18 Jun, 2019 at 1:23 pm

Estimadas/os: estamos experimentando con los chicos/as con clones. Les comparto videotutorial de creación de clones con Scratch.

 

 
Enlace web
Clones en Scratch
https://www.youtube.com/watch?v=XbtzypusOj0&t=8s

Me gusta 's comment · Responder
Ocultar 1 respuesta
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZJue 20 Jun, 2019 at 7:33 am

Gracias Sergio!   Ideal para el proyecto!  Muy claro el video.   

 
Me gusta 's comment · Responder
Imagen de perfil de Sergio Arciénaga
Sergio ArciénagaEditado · Jue 20 Jun, 2019 at 11:11 am

Inspirado con la propuesta de Sebastián de utilizar el sensor del volumen de sonido como disparador de eventos, les comparto la simulación de espantar las palomas a través de un "complejo mecanismo" sonoro (el popular ¡¡fuera bicho!! :-)

Videotutorial: EspantaPalomas
Archivo espantandopalomas.sb2120 KB
Me gusta 's comment3 · Responder
Ocultar las 3 respuestas
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZEditado · Vie 21 Jun, 2019 at 6:07 am

Hola Sergio, muy buena clase de scratch!!!

Felicitaciones y gracias por compartirlo!!!    

Lo voy a adaptar para los problemas que surgieron en dos de mis cursos...  pero cambiando palomas y el mecanismo sonoro,  popular: "fuiiira perro..."

 

 

 

 
Me gusta 's comment1 · Responder
Imagen de perfil de Sergio Arciénaga
Sergio ArciénagaLun 24 Jun, 2019 at 10:40 am

Ojo que lo tengo con copyright al "mecanismo sonoro" :)
Me gusta 's comment · Responder
Imagen de perfil de Mónica Regueiro
Mónica RegueiroVie 12 Jul, 2019 at 12:30 am

ja ja recien veo este mecanismo sonoro .. es lo mismo de mi palmada... jua jua 
Me gusta 's comment · Responder
Imagen de perfil de Cintia Pettigrew
Cintia PettigrewVie 21 Jun, 2019 at 10:08 am

Hola! Les comparto una actividad que trabaje con los chicos en la semana7. Tal vez a alguno le sirva. Les paso la actividad de la basura y el reciclado, por que es un problema que se repite en varios cursos, yo les plantee este tipo de actividad a cada curso segun su problema situado.

Les muestro a los chicos como queda la actividad resuelta y les dejo en su carpeta para que descarguen la incompleta para que la hagan. Lo que busco con la actividad es la reutilización de porciones de codigo, haciendo una pequeña modificacion, y luego lo relaciono tambien con lo trabajado en la primera parte de la clase que tenia que ver con los modelos de solucion que ellos debian buscar en el desafio, y si modificandolos se ajustaban o no a su problematica.

La actividad tiene varias cosas para programar y tambien para hacer nuevos disfraces. Me ha dado resultado. Aunque les cuesta entender!! a pesar que la programacion es básica, cambio de disfraz y caminar.

Espero les guste y les sirva
Mostrar menos
Archivo Juntar_la_basura.sb2498 KB
Archivo Juntar_la_basura-incompleto.sb2429 KB
Me gusta 's comment6 · Responder
Ocultar 1 respuesta
Imagen de perfil de Mónica Regueiro
Mónica RegueiroMar 25 Jun, 2019 at 12:06 am

que lindo tacho de basura!!
Me gusta 's comment · Responder
Imagen de perfil de Mónica Regueiro
Mónica RegueiroVie 21 Jun, 2019 at 10:30 pm

El encuentro del otro dia fue inspirador... miren la que página que encontré, como la del avatar pero construye super heroes  

https://superherotar.framiq.com/es/

los podemos bajar en png para usar directo en scratch. 

Lo unico que no encontre es como borrar un accesorio(bigote, nariz) o lo que fuera. quedan relindos

aqui ejemplo de  la adicta a startrek.

 
Archivo adjunto de imagen
Me gusta 's comment2 · Responder
Ocultar 1 respuesta
Imagen de perfil de Sergio Arciénaga
Sergio ArciénagaLun 24 Jun, 2019 at 10:47 am

Justo estoy empezando en Tacuarembó con grupos nuevos. Gracias
Me gusta 's comment · Responder
Imagen de perfil de Sergio Arciénaga
Sergio ArciénagaLun 24 Jun, 2019 at 10:46 am

Estimadísimos/as, les chusmeo una herramienta que me funcionó muy bien (se lleva bien con jabber) y permite dibujar sobre la pantalla para, por ej, resaltar alguna zona cuando se está utilizando Scratch (círculo, rectángulo, flecha). También es muy útil a la hora de hacer videotutoriales. Se llama Pointofix y su uso es gratuito. La pueden descargar desde http://www.pointofix.de/download.php (recomiendo la versión portable y la traducción al español). Saluditos
Mostrar menos
Me gusta 's comment3 · Responder
Ocultar las 3 respuestas
Imagen de perfil de Emiliano López
Emiliano LópezLun 24 Jun, 2019 at 12:00 pm

es excelente, muy buena recomendación
Me gusta 's comment · Responder
Imagen de perfil de Ariel Solovey
Ariel SoloveyLun 24 Jun, 2019 at 11:42 pm

Excelente recurso Sergio. Muchas gracias por compartirlo.
Me gusta 's comment · Responder
Imagen de perfil de Marcela Bonavia
Marcela BonaviaVie 9 Ago, 2019 at 9:13 am

Hola Sergio!! Me detallarías los pasos para descargar y configurar el idioma? Gracias
Me gusta 's comment · Responder
Imagen de perfil de Sabrina De Vita
Sabrina De VitaMar 9 Jul, 2019 at 7:30 pm

Hola a todos:

Les comparto las animaciones realizadas por los alumnos de 6 B de la escuela 123 de Canelones para presentar su problema situado: el riego de la huerta: 123canelones 

Algunas son mas complejas que otras dado que algunos alumnos ya sabian programar en Scratch y para otros era la primera vez. 

Saludos y espero que les gusten.
Me gusta 's comment1 · Responder
Ocultar las 4 respuestas
Imagen de perfil de Sergio Arciénaga
Sergio ArciénagaEditado · Mie 10 Jul, 2019 at 11:02 am

Felicitaciones Sabrina, la mezcla de trabajar con la página del MIT y Padlet es super. Gracias por inspirar
Me gusta 's comment1 · Responder
Imagen de perfil de Mónica Regueiro
Mónica RegueiroVie 12 Jul, 2019 at 12:47 am

Me gusto mucho la idea, pero me surge una duda, porque figuran todos como tuyos, vos los subis, o ellos lo codifican ya en scratch3 y solo lo comparten?
Me gusta 's comment · Responder
Imagen de perfil de Sabrina De Vita
Sabrina De VitaVie 12 Jul, 2019 at 8:25 am

Yo los subi. Ellos lo subieron a Crea como archivo
Me gusta 's comment · Responder
Imagen de perfil de Mónica Regueiro
Mónica RegueiroVie 12 Jul, 2019 at 9:05 am

gracias por responder. Queda divino para la presentacion final!!
Me gusta 's comment1 · Responder
Imagen de perfil de Alvar Leandro Maciel
Alvar Leandro MacielDom 21 Jul, 2019 at 4:23 pm

Lo dejé en la cafetería, pero creo que es más apropiado dejarlo acá

Un pequeño tuto sobre coordenadas con un pequeño juego
Enlace web
Coordenadas y minijuego: ponerle lunares a frank
https://scratch.mit.edu/projects/321070845

Me gusta 's comment5 · Responder
Ocultar 1 respuesta
Imagen de perfil de Sergio Arciénaga
Sergio ArciénagaMie 7 Ago, 2019 at 8:10 pm

Ese Frank cada vez se perfecciona más. Felicitalo de mi parte :)
Ya no me gusta 's comment1 · Responder
Imagen de perfil de Sergio Arciénaga
Sergio ArciénagaMie 7 Ago, 2019 at 8:12 pm

Comparto un pdf con una propuesta de programación colaborativa de un juego en Scratch.
Adobe PDF Trabajo_Colaborativo_con_Scratch_2.pdf1 MBVER
Me gusta 's comment1 · Responder
Imagen de perfil de Matias Bruno
Matias BrunoLun 12 Ago, 2019 at 8:50 am

Buenos dias! Comparto mi experiencia con los chiquilines de las esc. 172 y 2, les demonstre como operar con los comandos basicos trabajados hasta estas semanas (semanas 11 y 12, comienzos de produccion de las simulaciones). En base a este programita que no tiene ningun eje contextual, ellos pueden extraer una nocion super basica de que herramientas incluir para generar un juego con un minimo de tiempo predispuesto.

 

Espero el lineamiento de los bloques les sea efectivo a alguien mas para plantear la idea de juego! Ya que los chicos se entusiasman muchisimo al representar el movimiento mas cotidiano que se encuentran en los videojuegos hoy en dia (movimiento basado en las teclas W, A, S y D).

 

Saludos!
Mostrar menos
Archivo simulacion_juego_n1.sb2512 KB
Me gusta 's comment1 · Responder
Ocultar 1 respuesta
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZLun 19 Ago, 2019 at 9:30 pm

Muy buena idea, Gracias
Me gusta 's comment · Responder
Imagen de perfil de Natalia Zaragoza
Natalia ZaragozaMar 13 Ago, 2019 at 7:52 pm

Buenas tardes, Realicé este juego para incorporar bloques nuevos en las animaciones y junto a los chicos pensamos donde poder utilizar esas órdenes y aplicarlas a la programación de su problema situado. Es muy sencillo, yo se los mostré, jugamos y luego analizamos la secuencia utilizada y vimos alternativas de uso de patrones con las modificaciones necesarias según cada grupo. En la clase siguiente vimos las coordenadas y creamos mandalas (analizando los elementos de las figuras geométricas, medición de ángulos, relación entre repeticiones y giros, coordenadas) Con esta actividad también se engancharon y compartiendo la pantalla, yo les modificaba la programación y ellos trataban de hacer lo mismo. La idea de estas actividades es la incorporación de bloques que puedan ser reutilizados como patrones en otras programaciones. 

Por otro lado les quería comentar que hay una página de uso de simuladores con contenidos educativos muy piola por si quieren ver y utilizar para las programaciones. Se llama Phet
Mostrar menos
Archivo Si_sino.sb2196 KB
Me gusta 's comment1 · Responder
Ocultar las 2 respuestas
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZLun 19 Ago, 2019 at 9:30 pm

Gracias Natalia!.   
Me gusta 's comment · Responder
Imagen de perfil de Marcela Bonavia
Marcela BonaviaLun 30 Sep, 2019 at 11:09 pm

¡Qué lindo!
Me gusta 's comment · Responder
Imagen de perfil de Ariel Solovey
Ariel SoloveyEditado · Lun 19 Ago, 2019 at 7:33 pm

Hola a todos, les comparto una versión en Scratch para automatizar la limpieza del patio a través de un robotito.

El robot se maneja con las flechas de dirección (y presionando los números 1 y 2 para las funciones de limpieza) o apretando las teclas desde el control remoto.

La actividad puede hacerse en dos clases, o entregarles a los chiquilines ya una parte de la programación lista, por ejemplo el código de la función número 1... y que ellos tengan que realizar la programación de la función número 2 y los movimientos básicos con las flechas de dirección (más para 6to grado pero se puede adaptar para otros niveles también, es sólo una idea inicial).

Para la programación tienen que conocer el uso de movimientos básicos (apuntar en dirección y mover pasos), el envío de mensajes entre objetos y el uso de procedimientos, que en este caso se usaron para las tareas repetitivas.

Saludos para todos,
Mostrar menos
Archivo Robot_Control_Remoto_con_Procedimientos.sb2198 KB
Me gusta 's comment2 · Responder
Ocultar las 2 respuestas
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZLun 19 Ago, 2019 at 9:30 pm

Gracias Ariel por compartirlo!  ¡Muy bueno!
Me gusta 's comment · Responder
Imagen de perfil de Sergio Arciénaga
Sergio ArciénagaMar 3 Sep, 2019 at 1:33 pm

Gracias Ariel, muy inspirador tu trabajo. La idea del control remoto, te aviso, ya te la estoy copiando. Saludos
Me gusta 's comment · Responder
Imagen de perfil de Gisele Eliana Flesler
Gisele Eliana FleslerJue 5 Sep, 2019 at 3:30 pm

Buenas tardes! comparto un archivo de Scratch que hizo un alumno de la escuela Paysandu 94, este grupo es un 6to avanzado, muy completo!

Ellos plantearon que hay mucha basura en la escuela y la ordenanza sola no puede limpiar todo, asique quieren confeccionar un robot que la ayude
Archivo Luisa_bot_videojuego-TZ.sb2400 KB
Ya no me gusta 's comment3 · Responder
Ocultar las 2 respuestas
Imagen de perfil de Sabrina De Vita
Sabrina De VitaVie 6 Sep, 2019 at 3:50 pm

Muy bueno Gisele. Tengo dos grupos que tambien estan trabajando sobre el tema de la basura-
Me gusta 's comment1 · Responder
Imagen de perfil de Cintia Pettigrew
Cintia PettigrewDom 8 Sep, 2019 at 8:19 am

Muy lindo esta!!!!!!!
Me gusta 's comment1 · Responder
Imagen de perfil de Javier Castrillo
Javier CastrilloJue 19 Sep, 2019 at 9:55 am

Hola a todxs:

A pedido de Mónica les comparto una experiencia que hice en la semana 10. No la sistematizo porque les estaría mintiendo, salió porque en un grupo me pidieron que les enseñe a poner música y sonidos en las animaciones y de ahí se disparó a "hacer música con scratch". Por eso no pongo "objetivos"ni "actividades" ni cuestiones relativas a la planificación.
Primero charlamos acerca de lo que eran los instrumentos de percusión y los que producen notas musicales. Arrancamos por la batería y las partes "que no pueden faltar" en la misma y que son bombo, tambor, platillo "crash" y charleston. Les conté que scratch tenía grabados muchos de esos sonidos y que podíamos hacer nuestros propios instrumentos. Comenzamos entonces eligiendo un escenario (hay varios en la galería) y dibujando nosotros un bombo muy simple. Ahí podíamos ponerle el nombre de la banda, del grado, etc. Y le asignamos el sonido usando el bloque "tocar tambor X durante N pulsos" y para el bombo usamos el sonido 2. Se dispara el sonido (concepto de "evento") bien cliqueando sobre el bombo o con una tecla a elección ("barra espaciadora" en el ejemplo).

Así lo hicimos para el tambor, el crash y el charles al que hay que darle dos sonidos, uno para la situación de "abierto" y uno para el de "cerrado"  con dos teclas diferentes ("a" y "s"  en el caso que ilustro)

Luego de tener la batería completa importamos un objeto de la galería filtrando por "música" y allí hay varios instrumentos. en el ejemplo hay un piano. (Algunos me preguntaron para qué dibujamos los elementos de la batería "si ahí había" y la respuesta fue que "para que vean que también se puede dar sonido a cosas que dibujamos nosotros y que practiquemos a hacer nuestras propios objetos en lugar de siempre importar los que habían hecho otros".  Como el piano puede hacer sonar notas en lugar de "sonidos percusivos" les pasé una melodía simple y les dije que había que adivinar qué canción era. Allí me sorprendí en que en todos los grados conocen "Estrellita" pero no todos el "Feliz cumpleaños" así que les paso el hit "Estrellita" en el ejemplo que adjunto. 

La actividad salió muy bien y entusiasmó así que la reproduje en otros grados y gustó en todos, sin excepción. En algunos grados hubo euforia, aplausos, etc.

Variantes: La actividad según la pasé es para los grados promedio. En los grados menos avanzados, solamente llegamos a la batería. Los grados avanzados pidieron más, entonces duplicamos los instrumentos, les pusimos otros sonidos y tuvimos una banda completa. En algún caso hasta pusimos una cantante (o coro) y las voces las grabaron los propios alumnos. En los 6tos usé melodías de rock en lugar de canciones infantiles.

Espero que les guste, y cualquier cosa me preguntan, critican, etc. Saludos!
Mostrar menos
Archivo adjunto de imagen
Archivo adjunto de imagen
Archivo adjunto de imagen
Archivo adjunto de imagen
Me gusta 's comment3 · Responder
Ocultar 1 respuesta
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZLun 14 Oct, 2019 at 10:11 pm

Muy buena idea Javier!!!  la música es motivadora. 

 
Me gusta 's comment · Responder
Imagen de perfil de Mónica Regueiro
Mónica RegueiroMie 25 Sep, 2019 at 9:04 pm

Buenas! Estuve charlando con algunos, esta actividad comodin, que utilizo como diagnóstico cuando me ha tocado tomar cursos de reemplazo. 

Es a partir de una foto que ellos se sacan y trabajando con los bloques de apariencia que salen bichos de la boca. Los niños se divierten haciendolo.

La foto me sirve para identificar a los niños tambien. 

 
Archivo quecomi.sb2161 KB
quecomi.flv
quecomi.flv16 MB
Me gusta 's comment5 · Responder
Ocultar las 4 respuestas
Imagen de perfil de Cintia Pettigrew
Cintia PettigrewSab 28 Sep, 2019 at 5:46 pm

Esta muy bueno!!!
Me gusta 's comment1 · Responder
Imagen de perfil de Daniela Rosa Saldaña
Daniela Rosa SaldañaMar 1 Oct, 2019 at 8:52 am

Gracias Moni!!! 

Besos!!!
Me gusta 's comment · Responder
Imagen de perfil de Sergio Arciénaga
Sergio ArciénagaMie 9 Oct, 2019 at 12:32 pm

Ja, me parece que algo te cayó mal Moni. Genial idea!!
Me gusta 's comment · Responder
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZLun 14 Oct, 2019 at 10:09 pm

Moni!!!   Muy buena idea!!  agregaría un bloque para cepillar los dientes...jaja!
Me gusta 's comment · Responder
Imagen de perfil de Daniela Rosa Saldaña
Daniela Rosa SaldañaMar 1 Oct, 2019 at 9:03 am

Buenos días!!!

Quisiera mostrar un Scratch que hicieron los chiquilines de la escuela 108 de Florida para mostrar el proceso de la fotosíntesis, utilizando lo que han aprendido. Me encantó!!!
Archivo Fotosintesis__1.sb2466 KB
Me gusta 's comment4 · Responder
Imagen de perfil de Sabrina De Vita
Sabrina De VitaMar 1 Oct, 2019 at 10:52 am

Hola a todos:

Les comparto los videojuegos creados por la escuela 123 de Canelones relacionados con el tema de la huerta. Utilizaron sensores, operadores y variables. 

www.padlet.com/sabridv85/videojuegos123

Espero que les gusten.

 
Ya no me gusta 's comment3 · Responder
Ocultar 1 respuesta
Imagen de perfil de ADRIANA CLELIA GIMENEZ
ADRIANA CLELIA GIMENEZLun 14 Oct, 2019 at 10:10 pm

Muy bueno Sabrina!!!
Me gusta 's comment · Responder
Imagen de perfil de Sabrina De Vita
Sabrina De VitaJue 3 Oct, 2019 at 1:28 pm

En la escuela 78 de Salto también programamos videojuegos con Scratch. En esta caso estamos tratando de solucionar el problema de la basura. Aquí nuestras creaciones. https://lnkd.in/eyrFJfG
Ya no me gusta 's comment5 · Responder
Imagen de perfil de SAMUEL KOWALCZUK
SAMUEL KOWALCZUKEditado · Sab 19 Oct, 2019 at 10:49 am

Buenas

Comparto una experiencia con Scratch realizada con cuarto grado para articular con una propuesta sobre la Alimentación Saludable y el Aparato Digestivo. Es un juego que permite al alumno conocer la función de cada órgano y armar un puzzle del apararo digestivo.

Para simplificar el trabajo de los alumnos se subió a Crea el proyecto Scratch "en blanco" solamente los objetos y sin bloques de programación. Luego cada avance del proyecto se comparte en etapas (subida al finalizar la clase) para registro de los alumnos y el docente. Aproximadamente el tiempo total fue de 5 clases.

Saludos
Mostrar menos
Archivo juego_ApaDigestivo.sb2494 KB
Archivo adjunto de imagen
Archivo adjunto de imagen
Me gusta 's comment3 · Responder
Ocultar 1 respuesta
Imagen de perfil de Cintia Pettigrew
Cintia PettigrewMie 30 Oct, 2019 at 3:46 pm

Muy interesante!!!!

 
Me gusta 's comment · Responder
Imagen de perfil de Cintia Pettigrew
Cintia PettigrewDom 3 Nov, 2019 at 1:22 pm

Hola! Comparto algunas producciones de mis escuelas, todos son nivel basico

https://padlet.com/cintia_pettigrew_ceibal1/8yltb1wn5qdv

https://padlet.com/cintia_pettigrew_ceibal1/qchartjckpc

https://padlet.com/cintia_pettigrew_ceibal1/jy5yofg6gr68
Me gusta 's comment2 · Responder
Ocultar 1 respuesta
Imagen de perfil de Mónica Regueiro
Mónica RegueiroDom 3 Nov, 2019 at 8:52 pm

Las cianobacterias nos invaden como tema...  muy lindos
Me gusta 's comment1 · Responder
Imagen de perfil de Sebastián Tacundo
Sebastián TacundoMie 6 Nov, 2019 at 7:53 pm

Comparto uno de los vídeo juegos sobre el prob. Situado en formato de video. Luego colocare todos los sb2 de los mismos de cada escuela respectivamente. Saludos
VID-20191104-WA0050.mp4
VID-20191104-WA0050.mp48 MB
Me gusta 's comment2 · Responder
Imagen de perfil de Mariela Benvenutti
Mariela BenvenuttiVie 8 Nov, 2019 at 7:03 am

Hola a todos: 

Comparto los trabajos de 6A de la Escuela N° 93 de Canelones, excelente grupo y DA. 

http://linoit.com/users/marielabenvenutti/canvases/EscuelaN93 

Cariños, Mariela
Me gusta 's comment · Responder
Imagen de perfil de Vanesa Josefina Ester Rodriguez
Vanesa Josefina Ester RodriguezVie 15 Nov, 2019 at 3:46 pm

Les comprato unos videojuegos que crearon alumnos de la Esc. 123 de Canelones.

https://scratch.mit.edu/studios/25425726/
Me gusta 's comment · Responder
Imagen de perfil de Vanesa Josefina Ester Rodriguez
Vanesa Josefina Ester RodriguezVie 15 Nov, 2019 at 4:24 pm

Comparto una actividad de la creación de un videojuego que realicé con algunos grupos de 4to en éstas últimas clases de PC.

 
Archivo videojuego.html451 KBVER
Me gusta 's comment · Responder
Imagen de perfil de Emiliano López
Emiliano LópezMie 20 Nov, 2019 at 2:23 pm

Buenas, les comparto este jueguito que hicimos con los niños de 4to grado de la Esc 255 de Montevideo. Fue una idea de ellos que creo que terminó saliendo muy bien.

Saludos
Archivo brujita_completo.sb21 MB
Me gusta 's comment1 · Responder
Ocultar Navegador de comentario.
	
Historial de Comentarios
Comentario siguiente
Comentario anterior
Comentario 80 de 80
Posted by Emiliano López Mie 20 Nov, 2019 at 2:23 pm

    Privacidad Ceibal
    Blog de Schoology
    Política de privacidad
    Condiciones de uso

Schoology © 2019
