## Repositorio para definir y seguir tareas

![alt text](imagenes/tareasEnMente.redimensionado.jpg)

Este espació es para probar una metodología a fin de llevar a cabo la
distribución de tareas entre el equipo de Referentes Pedagógicos.  A
medida que crece el proyecto, crece la cantidad de tareas a llevar a
cabo por todo el equipo. Por otro lado, dada la caracetrística
asincrónica de nuestro trabajo. Depender solo de whatsApp dificulta
que encontremos alguna tarea asignada si no pudimos leer los chats
durente un tiempo.

Se propone utilizar la sección [Boards](https://gitlab.com/pedagogicoceibal/gestiondetareas/-/boards)
para agregar tareas (AKA [Issues](https://gitlab.com/pedagogicoceibal/gestiondetareas/issues))
y en ella definir estados y responsables

Por Ejemplo, en esta tabla podemos ver 3 tareas con diferentes estados y responsables.

|Tarea|Estado|Responsables|
|---  |---|---|
|Seguimiento DA xxx|Pendiente|Sergio, Adriana|
|Carpetas PT 14|Hecho|Mabel, Adriana|
|Desgrabación Multipunto|En proceso|Alvar|

* Columna 1: Tareas
* Columna 2: Estado
* Columna 3: Responsables

Lo interesante de sostener este proceso es que podemos contar con un
sistema que facilita la documentación concentrando tareas con recursos
que despues devienen documentos.

El sistema nos permite contar con una wiki, snnipets para hacer
pequeños recortes y guardar documentos que deviene páginas. Es muy
completo y también nos permite implemetar una metodología agíl para
llevar a cabo proyectos.


[![alt
text](imagenes/Captura_de_pantalla_de_2019-08-02_11-31-27.png "Pizarra
de
tareas")](https://gitlab.com/pedagogicoceibal/gestiondetareas/-/boards)

## Pasos para participar del repositorio

1. Sacarse una cuenta en [gitlab](https://gitlab.com)
2. Informar a Alvar o Sergio sus nombre de usuario



## Formato de texto

Gitlab puede usar multiples formatos para incluir textos en los
Issues. El más usado y común es Markdown. Un lenguaje de markado
simple que nos permite escribir con una sintaxis sencilla y luego el
sistema lo muestra con un formato legible y hermoso.  Por ejemplo, así
fue escrito este doc:


```markdown
## Repositorio para definir y seguir tareas

![alt text](imagenes/tareasEnMente.redimensionado.jpg)

Este espació es para probar una metodología a fin de llevar a cabo la
distribución de tareas entre el equipo de Referentes Pedagógicos.  A
medida que crece el proyecto, crece la cantidad de tareas a llevar a
cabo por todo el equipo. Por otro lado, dada la caracetrística
asincrónica de nuestro trabajo. Depender solo de whatsApp dificulta
que encontremos alguna tarea asignada si no pudimos leer los chats
durente un tiempo.

Se propone utilizar la sección [Boards](https://gitlab.com/pedagogicoceibal/gestiondetareas/-/boards)
para agregar tareas (AKA [Issues](https://gitlab.com/pedagogicoceibal/gestiondetareas/issues))
y en ella definir estados y responsables

Por Ejemplo, en esta tabla podemos ver 3 tareas con diferentes estados y responsables.

|Tarea|Estado|Responsables|
|---  |---|---|
|Seguimiento DA xxx|Pendiente|Sergio, Adriana|
|Carpetas PT 14|Hecho|Mabel, Adriana|
|Desgrabación Multipunto|En proceso|Alvar|

* Columna 1: Tareas
* Columna 2: Estado
* Columna 3: Responsables

Lo interesante de sostener este proceso es que podemos contar con un
sistema que facilita la documentación concentrando tareas con recursos
que despues devienen documentos.

El sistema nos permite contar con una wiki, snnipets para hacer
pequeños recortes y guardar documentos que deviene páginas. Es muy
completo y también nos permite implemetar una metodología agíl para
llevar a cabo proyectos.


[![alt
text](imagenes/Captura_de_pantalla_de_2019-08-02_11-31-27.png "Pizarra
de
tareas")](https://gitlab.com/pedagogicoceibal/gestiondetareas/-/boards)

```

Acá tienen una referencia completa de [Markdown en gitlab](https://docs.gitlab.com/ee/user/markdown.html#colored-code-and-syntax-highlighting)
