## Bienvenidos al Pad del Cuarto de capacitación de la fundación Sadosky

¡ATENCIÓN!: Este "pad" es un documento colaborativo que puede ser editado por cualquiera que tenga un LINK

¡ATENCIÓN! Este pad se borrará luego de 60 días de inactividad. No hay manera de recuperar este pad una vez borrado.

Agenda:
1. Presentación en el PAD
2. Orientaciones finales antes de comenzar las clases
3. Contenidos y estrategias para las primeras clases
4. Tareas pareja DR/DA
5. Tiempos y materiales
6. Copiar carpeta
7. Plan B



Presentación en el PAD:
    
Agregar una línea con nombre y si querés tu nick en las Redes

* Alvar Maciel/@amaciel
* Ezequiel Gentile Montes / @gentilmente 
* Graciela Graciano
* Leonel Mateo
* Andrés Pagotto / @andres.pagotto 
* Jose Daniel Villagran Lazzarone
* Ruth Teves
* Rosario Cabaleiro
* Claudia Moreno
* Alejandro Camara
* Catina Grosso
* Alejandra Sebastián
* Simonetta Sebastian
* Cordoba Victoria
* Pablo Visconti 
* Analía Cuenca
* Telma Figueroa
* Carolina Caceres
* Carolina Quevedo
* Norma Salcedo
* Corina Perez Garate
* veronica Brisoliz
* Bibiana Rivadeneira
* Agostina Espin 
* Roxana Maidana
* Sergio Arciénaga @sarcienaga
* Mabel Bermudez

### Orientaciones finales antes de comenzar las clases

#### Preguntas:
* ¿En las computadoras ya tienen descargados el Scratch?
  * Si. el 2
* una maestra me dijo que tenía el 1.4. puede ser?
  * Si puede ser, aveces lo tienen en otro menú. Pero en caso de que no esté por ningún lado, van a tener que bajarlo
  * O también se puede trabajar con Scratch 3 on line. 

* Si la clase dura idealmente 45 minutos. ¿Cuánto tiempo seria recomendado tomarnos para la metacognición?
  * Puede ser 5-10 minutos antes que termine la clase, comenzar el cierre. Por supuesto que el proceso de metacognición puede darse en distintos momentos  en los que resulte pertinente.
  * Gracias, entendido.
* ¿Cómo nos manejamos con el Plan B? Si fuera necesario
  * Ya vamos a ir a ello
* ¿Todas las computadoras de los alumnos tienen ubuntu?
  * si
  * ¿qué versión?
  * creo que la 12. O eso dicen los documentos
* ¿hay alguna ubicación específica para que guarden los proyectos en sus computadoras?
  * Tienen la carpeta Documentos y Descargas. Está bueno que guarden en Documentos


### Contenidos y estrategias para las primeras clases

#### Preguntas:

* en resumen: lo que tenemos que acordar esta semama con el docente es: con que propuesta comenzamos según el nivel del grupo clase.- Por ejemplo, lo más básico es el juego co-crear.- ¿como es este juego?, podemos explicarlo  
* ¿especificamente con que tema empezamos?
  * Con co-crear, o somos superheroes. Si no tienen el curso asignado Plan B 
* En la primera propuesta ¡¡Nos conocemos y pensamos en problemas!!, hay un ejemplo de cómo trabajar dicha propuesta (alternativa a Scratch).
* ej juego co-crear ¿no lo utilzamos entonces? 
  * Es una posibilidad para ver el trabajo en grupo y ver ¿como resuelven el problema de comunicación?
* No se utiliza el juego co-crear?? 
* y el avatar? lo usamos con 4to?
* Pra usar el avatar, ellxs lo hacen desde sus computadoras?
  * No necesariamente, pueden ser guiás para crear el avatar nuestro
* puedo usar avatar y no el dibujo de co-crear? Si eliges una actividad
* Si no se llega a escuchar bien el video de la luna, nos podemos tomar un tiempo para que la maestra pueda entrar en una de las compu si le pasamos el enlace?
* Si, suelo hace eso
* Tienen acceso a internet entonces en esas computadoras?
* Las computadoras de los chicos tienen acceso directo al Crea? allí es donde tienen que guardar los proyectos de Scratch?
  * Si, normalmente tienen un acceso directo con el icono de Crea. Allí, dentro de Pensamiento Computacional, pueden subir sus archivos .sb2

### Tareas pareja DR/DA

#### Preguntas

* Varias docentes aceptaron un tiempo de coordinación durante el recreo, luego de la clase con los niños. Debo cortar la comunicacion y volver  a llamar? (pregunto por el registro para el sector de registro de clases)
 * Es  Necesario cortar y reiniciar la videoconferencia
* Quién me habilita el curso en schoology? 
 * Los encargados de la administración de la plataforma


### Tiempos y materiales

#### Preguntas

* En que tiempo desarrrollan los desafios?
* Los desafios se resuelven en la semana. O sea que tienen ese tiempo para resolverlo
* El video del Agua en la luna por ejemplo, los mostramos en pantalla compartida?
* En cuanto al lightboot o pilabloques se lo enseñamos a usar nosotros en la clase?
* En los 6to. grados que ninca tuvieron , dariamos lo mismo que elos 4tos y 5tos? la misma pregunta.
  * sip
* Tengo 3 cursos seguidos en la misma escuela pero sin tiempo intermedio, empiezo 5 minutos despúes cada uno?
* Si no resuelven desafio se da mas tiempo o lo resolvemos de manera conjunta la clase siguiente?
	* si, eso lo vas a tener que ver con la DA para ver si conviene o no en el proyecto
* entonces la cuestión es sacar conclusiones de lo dificil que es, o los problemas que hay para, hacer un dibujo en equipo sin comunicarse?
 * si, y como los resolvemos, que estrategias ponemos en juego
* Bien!

* Entonces, debe quedar evidencia de la clase dada (por ejemplo capturas de pantalla o archivos) y evidencias del desafio?
 * sip
* ¿De que manera llegamos a Noviembre, les avisamos que no vamos a utilizar las placas microbit? porque muchas escuelas las recibieron y estan muy ansiosos. El año que viene se retoma?
 * esperemos que si :)

* Hoy una maestra me comentó que le llego una caja de "pensamiento computacional", en la foto tenía materiales como plasticolas, fibras y demás. Son 6 grupos y llegaron 2 nomas, es para compartir entre todos entonces?



### Copiar carpeta

#### Preguntas
 
* Quien crea los grupos y copia las carpetas del alumno? Como accedemos?
 * Los grupos los crean los encargados en la gestión de los grupos (nosotros no). Cada uno de nosotros copiamos las carpetas a dichos cursos. Hay un video tutorial que explica cómo hacerlo. En dichas carpetas nosotros podemos hacer las modificaciones que consideremos (siempre sin cambiar la propuestaque nos baja desde Ceibal)- 
* y como entramos a crea en cada curso para hacer la copia? Ahora lo aclaramos.
* esto me parece muy importante!!! por favor aclarar

* ok
* Pero entoncesSchoology y CreA2 es lo mismo?
* Sip

* La opción de Recursos en Crea... es personal? o sea, sóloyo tengo acceso?
 * Si, cuando creas algo ahí podés elegir si el recurso es público o no

* Podemos ver (una vista) como organizan las carpetas de cada grado en Crea?
 * si, tambien está el tutorial [https://www.youtube.com/watch?v=rQPNh3jgeLg](https://www.youtube.com/watch?v=rQPNh3jgeLg)


* Cuando ingreso al desafio veo 4 opciones, cuando copio este desafio en la carpeta de los alumnos, elijo uno de ellos y borro los otros 3?
 * Depente, si acordás con la DA que opción eligen copiale la que decidan. Si acuerdan entre todes y toman 2 opciones de las 4 le copias los 2
* ¿quién da el acceso a los cursos? ¿asignación? siendo que ya tenemos la asignación y se comienza el lunes, no deberiamos tenerlo ya?
 * El acceso lo da Ceibal y el requerimiento empieza cuando confirmamos nuestras asignaciones.
* los chicos ya pueden entrar a crea y ver su curso?
 * Si, es su aula virtual, están en él desde el inico del año
 * Si se comienza el lunes, con los cursos del lunes vamos por el plan B?
 * si no tenés los cursos, si.

* las evidencias que cuelgan los chicos, nosotros podemos moverlas de lugar (por si se equivocaron)??
 * Es una tarea que debemos realizar con los alumnos, la lectura de la consignas y las respuestas en el lugar correspondiente, atender la ortografía es otra tarea importante



* la carpeta PC se crea antes de la primer VC.??
 * si. en cuanto tengas acceso al curso en crea2 creala y podés copiar y editar la carpeta de la primer propuesta
* ok.
* Nos pueden mostrar cómo se trabaja con el foro?
* Para cada clase tenemos que crear un foro? Las evidencias las tienen que subir al foro?
 * si, cada propuesta de trabajo tiene su carpeta con sus foros


* Todas las maestras saben entrar a la plataforma?
 * Si, en principio
* Como le creo el foro?
* como creas las carpetas. elegís la opción agregar tema de discusión https://www.dropbox.com/s/9hsqggj2exztcxx/Captura%20de%20pantalla%20de%202019-07-31%2020-02-29.png?dl=0
ok


* Las ceibalitas son las compu que tienen lxs chicxs de 4 a 6° año?
 * si

* Tenemos que contarles nosotros sobre los distitos talleres de robotica,programacion,etc?
 * Comentarlo al menos con la DA y que ella defina que hace, si se inscribe o no
* Hoy le pregunte en el grupo de Whatsapp, pero ahora me acorde, el problema de las Cianobacterias por que no es recomendable?

* Se puede mechar para la presentación el juego 1 y 2, es decir, hacerlos dibujar por grupos en un hoja algún superhéroe que reuna las cualidades y fortalezas que identifiquen a ese grupo?

### Plan B

#### Preguntas
* cuando usamos el plan B?? 
 * Cuando no tenemos los cursos en crea2

* Si fracasa co-crear que hacemos?
 * **[PLAN B](https://docs.google.com/document/d/1Pz_Im0JPtqDuv9CjIOeT1x1c0ElUEtUo8xaixRtPjOY/edit?usp=sharing)**
* Me refiere a qué pasa si los alumnos les cuesta sacar conclusiones o incluso saber qué dibujar. Es una consigna muy atractiva pero puede no haber respuesta
 * Tranquilos, vamos llevando la clase, puede que entren en tarea con el tiempo y podés pasar al problema de la Luna
* Te referis a pasar el video?
 * si o que lo vean en su compu y pensar como llevar agua a la luna
* Por que crear el avatar nuestro y no que creen el propio de cada grupo?
 * porque abre un juego entre ellos y nosotros en la cual se establece un punto de confianza y de juego en el momento en que ellos interpretan nuestra imagen
* podemos invitarlos a que luego hagan el suyo despues?

### Resumen de Carina para el primer encuentro: (5to y 6to.)
Nos presentamos :

- usamos un juego con  co crear  con una palabra disparadora acordada con la DA
- Sacamos una foto y la subimos al foro
- Pensamos en un problema situado para la clase que viene
        
- 4to, grado nos presentamos podemos usar el avatar
- Lo subimos nosotros al foro
- Pensamos en un problema situado como desafio para la clase siguiente
        
- Si los 6to. ya tuvieron pensamiento computacional despues de la presentacion podemos hacer un coloquio y que luego ellos se presenten por medio de una foto en scratch y la suban al foto
        
* ¿al avatar lo hacen a mano? 
 * nop. En el doc plan b está el link y el paso a paso https://pad.riseup.net/redirect#https%3A//docs.google.com/document/d/1Pz_Im0JPtqDuv9CjIOeT1x1c0ElUEtUo8xaixRtPjOY/edit%3Fusp%3Dsharing  
* ok gracias
        
